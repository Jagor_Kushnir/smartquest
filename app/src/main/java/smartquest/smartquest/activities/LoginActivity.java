package smartquest.smartquest.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import smartquest.smartquest.R;

/**
 * Created by uniq on 28.04.2018.
 */

public class LoginActivity extends Activity {

    private boolean login;

    private EditText phoneNumber;
    private Button RLButton;
    private TextView loginLink;
    private TextView registerLink;

    private String udid;
    private String phoneNumberTxt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        //setting theme from splashscreen
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);

        checkIfRegisteredForUI();
        initialize();
    }

    //Checking if user was previously registered
    public void checkIfRegisteredForUI(){
        SharedPreferences preferences = getSharedPreferences(getString(R.string.user_info), MODE_PRIVATE);

        if (preferences != null){
            String userId = preferences.getString(getString(R.string.user_id), null);
            if (userId != null && !userId.equals("")){
                setContentView(R.layout.activity_login_login);
                login = true;
            }else {
                setContentView(R.layout.activity_login_register);
                login = false;
            }
        }
    }

    public void initialize(){
        phoneNumber = (EditText) findViewById(R.id.phone_number);
        RLButton = (Button) findViewById(R.id.r_l_btn);
        loginLink = (TextView) findViewById(R.id.login_link);

        RLButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                udid = Settings.Secure.getString(getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                phoneNumberTxt = phoneNumber.getText().toString();
                Log.d("MY_TAG", "onClick: " + udid + "\n" + "number: " + phoneNumberTxt);

                // TODO: 4/30/2018 here we need to serialize the data to json and send a request
            }
        });

        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(login == false) {
                    setContentView(R.layout.activity_login_login);
                    login = true;
                    initialize();
                }else{
                    setContentView(R.layout.activity_login_register);
                    initialize();
                    login = false;
                }
            }
        });

    }
}
