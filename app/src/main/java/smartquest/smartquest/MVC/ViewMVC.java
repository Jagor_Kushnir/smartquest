package smartquest.smartquest.MVC;

import android.os.Bundle;
import android.view.View;

public interface ViewMVC {

    public View getRootView();

    public Bundle getViewState();

}
