//
//  Models.swift
//  SmartQuest
//
//  Created by Pavel on 25/11/2017.
//  Copyright © 2017 Pavel Khanukov. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
import CoreLocation

class Location : Object
{
    @objc dynamic var latitude = 0.0
    @objc dynamic var longitude = 0.0
    @objc dynamic var addressText : String = ""
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    var imagePath : String {
        return "\(latitude)" + "\(longitude)"
    }
}

//MARK:- Scenatrio
class Scenario : Object
{
    @objc dynamic var id : Int = 0
    @objc dynamic var name : String = "name"
    @objc dynamic var fullDescription : String = "description"
    @objc dynamic var shortDescription : String = "description"
    @objc dynamic var type : Int = 0
    @objc dynamic var price : Double = 0
    @objc dynamic var difficult : Int = 3
    @objc dynamic var startDate : String = "start_date"
    @objc dynamic var endDate : String = "end_date"
    @objc dynamic var minimumPlayers : Int = 1
    @objc dynamic var imageUrl : String = "image_url"
    @objc dynamic var rate : Double = 0
    @objc dynamic var votes : Int = 0
    @objc dynamic var lang : Language?
    @objc dynamic var city : City?
    @objc dynamic var distance: Int = 0
    @objc dynamic var time: Int = 0
 
    var news = LinkingObjects(fromType: News.self, property: "scenario")
    
    var scenarioType : ScenarioType
    {
        guard let newsType = news.first?.newsType else {
            return ScenarioType.standart
        }
        
        switch newsType {
        case .news:
            return ScenarioType.standart
        case .newScenario:
            return ScenarioType.new
        case .promoScenario:
            return ScenarioType.promo
        }
    }
    
    var coordinates = List<Location>()
    var stages     = List<Stage>()
    var facts      = List<Fact>()
    var categories = List<ScenarioCategory>()
    
    var isDetailed : Bool {
        return stages.count > 0
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override static func ignoredProperties() -> [String] {
        return ["isDetailed","scenarioType"]
    }
    
    func getPriceString() -> String{
        guard price == 0 else {
            return String.init(format: "$%.2f", price)
        }
        
        return "GET"
    }
}

extension Scenario {
    class func create(WithID id : Int) -> Scenario {
        let scenario = Scenario.init()
        scenario.id = id
        return scenario
    }
}

//MARK:- Scenario Tags

class ScenarioCategory : Object {
    @objc dynamic var id : Int = 0
    @objc dynamic var categoryDescription : String = "name"
    override static func primaryKey() -> String? {
        return "id"
    }
    
    var listOfNames = List<ScenarioCategoryNames>()
    
    class func create(WithID id : Int) -> ScenarioCategory {
        let scenario = ScenarioCategory.init()
        scenario.id = id
        return scenario
    }
    
    func getName() -> String {
        guard let user = StorageManager.getUser() else {
            return findNameByCode(LangCode: "EN") ?? "Missed name for ScenarioCategory ID \(id)"
        }
        
        guard let lang = user.language else {
            return findNameByCode(LangCode: "EN") ?? "Missed name for ScenarioCategory ID \(id)"
        }
        
        return findNameByCode(LangCode: lang.code ?? "EN") ?? "Missed name for ScenarioCategory ID \(id)"
    }
    
    func findNameByCode(LangCode code : String) -> String? {
        return listOfNames.first(where: { (catName) -> Bool in
            return catName.langCode.lowercased() == code.lowercased()
        })?.name
    }
}

class ScenarioCategoryNames : Object {
    
    @objc dynamic var id : String = ""
    @objc dynamic var name : String = "name"
    @objc dynamic var langCode : String = ""
    override static func primaryKey() -> String? {
        return "id"
    }
    
    class func create(WithID catNameID : String) -> ScenarioCategoryNames {
        let result = ScenarioCategoryNames()
        result.id = catNameID
        return result
    }
}

//MARK:- Stage
class Stage : Object
{
    @objc dynamic var id : Int = 0
    @objc dynamic var name = "name"
    @objc dynamic var fullDescription : String = "description"
    @objc dynamic var type : Int = 0
    @objc dynamic var duration : String = "duration"
    @objc dynamic var order : Int = 0
    
    var scenarios = LinkingObjects(fromType: Scenario.self, property: "stages")
    var tasks = List<Task>()
    
    func getTask(Order index : Int) -> Task {
        return tasks.sorted(byKeyPath: "order")[index]
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Stage
{
    class func create(WithID id : Int) -> Stage
    {
        let scenario = Stage.init()
        scenario.id = id
        return scenario
    }
}

//MARK:- Task
class Task : Object
{
    @objc dynamic var id : Int = 0
    @objc dynamic var scenarioID : Int = 0
    @objc dynamic var type : Int = 0
    @objc dynamic var duration : String = "50 min"
    @objc dynamic var order : Int = 0
    @objc dynamic var role_id : Int = 0
    @objc dynamic var title : String = "title"
    @objc dynamic var imageURL : String?
    @objc dynamic var coords : String?
   

    @objc dynamic var fact : Fact?
    var wrongAnswers = List<String>()
    var answers = List<Answer>()
    var sections = List<Section>()
    var hints = List<Hint>()
    var stages = LinkingObjects(fromType: Stage.self, property: "tasks")

    func getSections() -> [Section] {
        return Array(sections.sorted(byKeyPath: "order"))
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Task
{
    class func create(WithID id : Int) -> Task
    {
        let task = Task.init()
        task.id = id
        return task
    }
}

//MARK:- Section
class Section : Object
{
    @objc dynamic var type : String = ""
    @objc dynamic var order : Int = 0
    
    @objc dynamic var leftSide : SectionData?
    @objc dynamic var rightSide : SectionData?
    @objc dynamic var centerSide : SectionData?
    
    func sectionType() -> SectionType {
        guard let existType = SectionType.init(rawValue: self.type) else {
            debugPrint("unknow type: \(self.type)"); return .unknow
        }
        
        return existType
    }
    
    enum SectionType : String {
        case text = "only_text"
        case contentLeft = "content_left"
        case contentRight = "content_right"
        case contentBetweenText = "content_between_text"
        case fact = "fact"
        case unknow = "unknow"
        case map = "map"
      //  case content = "content_between_text"
    }
    
    func getText() -> String {
        guard let string = centerSide?.text else {
            guard let string = leftSide?.text else {
                guard let string = rightSide?.text else {
                    return ""
                }
                return string
            }
            return string
        }
        return string
    }
}

//MARK: SectionData
class SectionData : Object
{
    @objc dynamic var text : String?
    @objc dynamic var type : String!
    @objc dynamic var note : String?
    @objc dynamic var url  : String?
    
    var coordinates = List<Location>()
    
    enum ContentType : String {
        case image = "image"
        case fact = "fact"
        case map = "map"
    }
}

extension SectionData
{
    class func create(WithData data : JSON) -> SectionData?
    {
        guard data.count != 0 else { return nil }
        let sectionData = SectionData.init()
        
        
        guard let points = data["points"].array else {
            sectionData.type = data[ScenarioConstants.content_type].stringValue
            sectionData.url = data[ScenarioConstants.content_url].string
            sectionData.note = data[ScenarioConstants.content_note].string
            sectionData.text = data[ScenarioConstants.text].string
            
            return sectionData
        }
        
        sectionData.type = SectionData.ContentType.map.rawValue
        
        points.forEach { (point) in
            let location = Location()
            location.latitude = point["latitude"].doubleValue
            location.longitude = point["longitude"].doubleValue
            ParseManager.coordinates(forCoordinates: location.coordinate, forLocation: location)
            sectionData.coordinates.append(location)
        }
        
        return sectionData
    }
}

//MARK:- Hint
class Hint: Object
{
    @objc dynamic var id : Int = 0
    let taskts = LinkingObjects(fromType: Task.self, property: "hints")
    var sections = List<Section>()
    var type : Int = 0
    var order : Int = 0
    var imageURL : String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Hint
{
    class func create(WithID id : Int) -> Hint
    {
        let hint = Hint.init()
        hint.id = id
        return hint
    }
}

//MARK:- Fact
class Fact : Object
{
    @objc dynamic var id : Int = 0
    @objc dynamic var imageURL : String?
    let taskts = LinkingObjects(fromType: Task.self, property: "fact")
    var sections = List<Section>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Fact
{
    class func create(WithID id : Int) -> Fact
    {
        let answer = Fact.init()
        answer.id = id
        return answer
    }
}

//MARK:- Answer

class Answer: Object
{
    @objc dynamic var id : Int = 0
    @objc dynamic var type : Int = 1
    @objc dynamic var text : String?
    @objc dynamic var specialText : String?
    @objc dynamic var isRight : Bool = true
    
    let taskts = LinkingObjects(fromType: Task.self, property: "answers")
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Answer
{
    class func create(WithID id : Int) -> Answer
    {
        let answer = Answer.init()
        answer.id = id
        return answer
    }
}

//MARK:- Language
class Language : Object
{
    @objc dynamic var code : String?
    @objc dynamic var id : Int = 0
    @objc dynamic var name : String?
    
    class func create(WithID langID : Int) -> Language
    {
        let result = Language()
        result.id = langID
        return result
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

//MARK:- City
class City : Object
{
    @objc dynamic var id : Int = 0
    @objc dynamic var name : String = ""
    @objc dynamic var shortName : String?
    @objc dynamic var imageURL : String?
    
    var listOfNames = List<CityName>()
    
    class func create(WithID cityID : Int) -> City {
        let result = City()
        result.id = cityID
        return result
    }
    
    func getName() -> String {
        guard let user = StorageManager.getUser() else {
           return findNameByCode(LangCode: "EN") ?? "Missed name for city ID \(id)"
        }
        
        guard let lang = user.language else {
            return findNameByCode(LangCode: "EN") ?? "Missed name for city ID \(id)"
        }
        
        return findNameByCode(LangCode: lang.code ?? "EN") ?? "Missed name for city ID \(id)"
    }

    func findNameByCode(LangCode code : String) -> String? {
        return listOfNames.first(where: { (cityName) -> Bool in
            return cityName.langCode.lowercased() == code.lowercased()
        })?.name
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

class CityName : Object {
    @objc dynamic var id : String = "cityID+LangCode" //131EN
    @objc dynamic var name : String = ""
    @objc dynamic var langCode : String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    class func create(WithID cityNameID : String) -> CityName {
        let result = CityName()
        result.id = cityNameID
        return result
    }
}

//MARK:- User
class User : Object
{
    @objc dynamic var userName : String = ""
    @objc dynamic var city : City?
    @objc dynamic var phoneNumber : String = ""
    @objc dynamic var email : String = ""
    @objc dynamic var userID : String = ""
    @objc dynamic var userType: Int = -1
    @objc dynamic var language: Language?
    
    class func create(WithID userID : String) -> User
    {
        let user = User()
        user.userID = userID
        return user
    }
    
    override static func primaryKey() -> String? {
        return "userID"
    }
}

extension User{
    
    //P - is parameter
    static let P_USER_NAME        = "user_name"
    static let P_USER_EMAIL       = "user_email"
    static let P_USER_CITY_ID     = "user_city_id"
    static let P_USER_LANGUAGE_ID = "user_language_id"
}

//MARK:- News
enum NewsType : Int
{
    case news = 0
    case promoScenario = 1
    case newScenario = 2
}

class News : Object
{
    @objc dynamic var newsID : Int = -1
    @objc dynamic var name : String = ""
    @objc dynamic var type : Int = 0
    @objc dynamic var imageURL : String = ""
    @objc dynamic var text : String = ""
    
    @objc dynamic var language : Language?
    @objc dynamic var city : City?
    @objc dynamic var scenario : Scenario?
  
    var newsType : NewsType {
        return NewsType.init(rawValue: type) ?? .news
    }
    
    class func create(WithID newsID : Int) -> News
    {
        let new = News()
        new.newsID = newsID
        return new
    }
    
    override static func primaryKey() -> String? {
        return "newsID"
    }
}

//MARK:- Orders
class Order : Object
{
    @objc dynamic var orderID : Int = -1
    @objc dynamic var paymentID : Int = -1
    @objc dynamic var scenarioID : Int = 0
    @objc dynamic var status : Int = 0
    
    override static func primaryKey() -> String? {
        return "orderID"
    }
}

extension Order
{
    class func getOrder(WithID orderID : Int) -> Order? {
        return RealmService.shared.realmConnection.object(ofType: Order.self, forPrimaryKey: orderID)
    }
    
    class func createOrder(WithID orderID : Int) -> Order {
        guard let order = getOrder(WithID: orderID) else {
            let order = Order()
            order.orderID = orderID
            return order
        }
        
        return order
    }
}
