//
//  ScenarioFilters.swift
//  SmartQuest
//
//  Created by Pavel on 10/02/2018.
//  Copyright © 2018 Pavel Khanukov. All rights reserved.
//

import Foundation
import RealmSwift

enum ScenarioFilterType : Int {
    
    static let allFilters : [ScenarioFilterType] = [.news,.popular,.scenarioCategories,.city,.language,.promo,.difficult,.distance,.minimumPlayer,.time,.completed]
    
    case news
    case popular
    case city
    case language
    case promo
    case difficult
    case distance
    case minimumPlayer
    case time
    case completed
    case scenarioCategories
}

enum ScenarioFilterTypeNames : String
{
    case news = "Новые"
    case popular = "Популярные"
    case city = "Города"
    case language = "Языки"
    case promo = "Ацкии"
    case difficult = "Сложность"
    case distance = "Расстояние"
    case minimumPlayer = "Количество игроков"
    case time = "Время"
    case completed = "Пройденые"
    case scenarioCategories = "Категория"
    
    static func getNameByType(FilterType type : ScenarioFilterType) -> String {
        switch type {
        case .news: return ScenarioFilterTypeNames.news.rawValue
        case .popular: return ScenarioFilterTypeNames.popular.rawValue
        case .city: return ScenarioFilterTypeNames.city.rawValue
        case .language: return ScenarioFilterTypeNames.language.rawValue
        case .promo: return ScenarioFilterTypeNames.promo.rawValue
        case .difficult: return ScenarioFilterTypeNames.difficult.rawValue
        case .distance: return ScenarioFilterTypeNames.distance.rawValue
        case .minimumPlayer: return ScenarioFilterTypeNames.minimumPlayer.rawValue
        case .time: return ScenarioFilterTypeNames.time.rawValue
        case .completed: return ScenarioFilterTypeNames.completed.rawValue
        case .scenarioCategories: return ScenarioFilterTypeNames.scenarioCategories.rawValue
        }
    }
}

class ScenarioFilterValue : Object
{
    class func createWithValue(value : Int) -> ScenarioFilterValue
    {
        let filterValue = ScenarioFilterValue()
        filterValue.value = value
        return filterValue
    }
    
    @objc dynamic var value : Int = 0
}

class ScenarioFilter : Object
{
    var filterType : ScenarioFilterType {
        return ScenarioFilterType.init(rawValue: filterID)!
    }
    
    var filterName : String {
        return ScenarioFilterTypeNames.getNameByType(FilterType: filterType)
    }
 
    @objc dynamic var filterID : Int = 0
    
    var filterValues = List<ScenarioFilterValue>()
    
    class func create(WithType : ScenarioFilterType, value : Int = -1) -> ScenarioFilter {
        
        guard let filter = StorageManager.getFilter(byType: WithType) else {
            let filter = ScenarioFilter()
            filter.filterID = WithType.rawValue
            filter.overrideValues(withValue: value)
            return filter
        }
        
        filter.overrideValues(withValue: value)
        return filter
    }
    
    func removeValue(value : Int = 0)
    {
        RealmService.shared.writeBlock {
            guard let filterValue = self.filterValues.filter("value == %ld", value).first else {
                return
            }
            
            guard let index = self.filterValues.index(of: filterValue) else {
                return
            }
            
            self.filterValues.remove(at: index)
        }
    }
    
    func overrideValues(withValue : Int)
    {
        RealmService.shared.writeBlock {
            self.filterValues.removeAll()
        }
        
        self.addValue(value: withValue)
    }
    
    func addValue(value : Int = 0)
    {
        RealmService.shared.writeBlock {
            let newFilter = ScenarioFilterValue.createWithValue(value: value)
            self.filterValues.append(newFilter)
        }
    }
    
    @objc open override class func primaryKey() -> String?
    {
        return "filterID"
    }
    
    @objc open override class func ignoredProperties() -> [String]
    {
        return ["filterType", "filterName"]
    }
}

