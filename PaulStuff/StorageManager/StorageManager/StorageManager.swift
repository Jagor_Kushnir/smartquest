//
//  StorageManager.swift
//  SmartQuest
//
//  Created by Pavel on 25/11/2017.
//  Copyright © 2017 Pavel Khanukov. All rights reserved.
//

import Foundation
import RealmSwift

class StorageManager
{
    static let realmService = RealmService.shared
    
    
    class func getNews(WithID id : Int) -> News?
    {
        return realmService.realmConnection.object(ofType: News.self, forPrimaryKey: id)
    }
    
    class func getAllNews() -> Results<News>
    {
        return realmService.realmConnection.objects(News.self)
    }
    
    class func removeNews(_ newsID : Int)
    {
        guard let news = getNews(WithID: newsID) else { return }
        realmService.writeBlock {
            realmService.realmConnection.delete(news)
        }
    }
    
    class func getLanguage(WithID id : Int) -> Language?
    {
        return realmService.realmConnection.object(ofType: Language.self, forPrimaryKey: id)
    }
    
    class func getAllLanguages() -> Results<Language>
    {
        return realmService.realmConnection.objects(Language.self)
    }
    
    class func getAllScenarioCategory() -> Results<ScenarioCategory> {
        return realmService.realmConnection.objects(ScenarioCategory.self)
    }
    
    class func removeLanguage(_ langID : Int)
    {
        guard let lang = getLanguage(WithID: langID) else { return }
        realmService.writeBlock {
            realmService.realmConnection.delete(lang)
        }
    }
    
    class func getCity(WithID id : Int) -> City? {
        return realmService.realmConnection.object(ofType: City.self, forPrimaryKey: id)
    }
    
    class func getCityName(WithID id : String) -> CityName? {
        return realmService.realmConnection.object(ofType: CityName.self, forPrimaryKey: id)
    }
    
    class func getAllCities() -> Results<City>
    {
        return realmService.realmConnection.objects(City.self)
    }
    
    class func removeCity(_ cityID : Int)
    {
        guard let city = getCity(WithID: cityID) else { return }
        realmService.writeBlock {
            realmService.realmConnection.delete(city)
        }
    }
}

//MARK:- Filters
extension StorageManager
{
    private class func createFilters()
    {
        ScenarioFilterType.allFilters.forEach { (filterType) in
            guard getFilter(byType: filterType) == nil else { return }
            let filter = ScenarioFilter.create(WithType: filterType)
            realmService.writeBlock {
                realmService.realmConnection.add(filter, update: true)
            }
        }
    }
    
    class func getFilter(byType : ScenarioFilterType) -> ScenarioFilter?
    {
        return realmService.realmConnection.object(ofType: ScenarioFilter.self, forPrimaryKey: byType.rawValue)
    }
    
    class func getFilters() -> Results<ScenarioFilter>
    {
        let result = realmService.realmConnection.objects(ScenarioFilter.self)
        
        guard result.count == ScenarioFilterType.allFilters.count else {
            StorageManager.createFilters()
            return StorageManager.getFilters()
        }
        
        return result
    }
}

//MARK:- Scenario
extension StorageManager
{
    class func getScenarios(WithFilters filters : [ScenarioFilter]) -> [Scenario]
    {
        var results = [Scenario]()
//        let quests = realmService.realmConnection.objects(Scenario.self)
//
//        filters.forEach { (filter) in
//            let filterType = filter.filterType
//
//            var filteredQuestsResults : Results<Scenario>?
//            var filteredQuestsArray : [Scenario]?
//
//            switch filterType
//            {
//
//            case .news:
//                filteredQuestsArray = quests.filter({ (scenario) -> Bool in
//                    return scenario.scenarioType == .new
//                })
//            case .popular:
//                filteredQuestsResults = quests.filter("rate > 4")
//            case .city:
//                filteredQuestsResults = quests.filter("city.id == %ld", filter.value)
//            case .language:
//                filteredQuestsResults = quests.filter("lang == %ld", filter.value)
//            case .promo:
//                filteredQuestsArray = quests.filter({ (scenario) -> Bool in
//                    return scenario.scenarioType == .new
//                })
//
//            case .difficult:
//                filteredQuestsResults = quests.filter("difficult >= %ld", filter.value)
//            case .distance:
//                break
//
//            case .minimumPlayer:
//                filteredQuestsResults = quests.filter("minimumPlayer >= %ld", filter.value)
//            case .time:
//                break
//            case .completed:
//                break
//            }
//
//            guard let result = filteredQuestsArray else {
//                guard let result = filteredQuestsResults else {
//                    return
//                }
//
//                results.append(contentsOf: result)
//                results = uniqueElementsFrom(array: results)
//                return
//            }
//
//            results.append(contentsOf: result)
//            results = uniqueElementsFrom(array: results)
//        }
//
        return results
    }

    
    class func getNewScenarios() -> [Scenario]
    {
        let quests = realmService.realmConnection.objects(Scenario.self)
        
        let result = quests.filter({ (scenario) -> Bool in
            return scenario.scenarioType == .new
        })
        
        return Array(result)
    }
    
    class func getScenatio(WithID id : Int) -> Scenario?
    {
        return realmService.realmConnection.object(ofType: Scenario.self, forPrimaryKey: id)
    }
    
    class func getAllScenarios() -> Results<Scenario>
    {
        return realmService.realmConnection.objects(Scenario.self)
    }
    
    class func getPopularScenarios(limit : Int = 0) -> [Scenario]
    {
        let result = realmService.realmConnection.objects(Scenario.self).filter("type == 4")
        
        guard limit > 0 else {
            return Array(result)

    }
        
        let limit = min(limit, result.count)
        
        let range = 0..<limit
        var resultArray : [Scenario] = []
        range.forEach { (indexOfScenario) in
            resultArray.append(result[indexOfScenario])
        }
        
        return resultArray
    }
    
    class func removeScenario(_ ScenarioID : Int) {
        guard let scenario = getScenatio(WithID: ScenarioID) else { return }
        realmService.writeBlock {
            realmService.realmConnection.delete(scenario)
        }
    }
}

//MARK:- Scenario Categories
extension StorageManager {
    class func getScenarioCategory(WithID id : Int) -> ScenarioCategory? {
        return realmService.realmConnection.object(ofType: ScenarioCategory.self, forPrimaryKey: id)
    }
    
    class func removeScenarioCategory(_ ScenarioCategoryID : Int) {
        guard let scenarioCategory = getScenarioCategory(WithID: ScenarioCategoryID) else { return }
        realmService.writeBlock {
            realmService.realmConnection.delete(scenarioCategory)
        }
    }
    
    class func getScenarioCategoryName(WithID id : String) -> ScenarioCategoryNames? {
        return realmService.realmConnection.object(ofType: ScenarioCategoryNames.self, forPrimaryKey: id)
    }
}

//MARK: - User
extension StorageManager {
    class func getUser(WithID id : String) -> User?
    {
        return realmService.realmConnection.object(ofType: User.self, forPrimaryKey: id)
    }
    
    class func getUser() -> User?
    {
        let userId = LoginManager.getUserId()
        
        return realmService.realmConnection.object(ofType: User.self, forPrimaryKey: userId)
    }
}

//MARK:- Stage
extension StorageManager
{
    class func getStage(WithID id : Int) -> Stage? {
        return realmService.realmConnection.object(ofType: Stage.self, forPrimaryKey: id)
    }
}

//MARK:- Task
extension StorageManager
{
    class func getTask(WithID id : Int) -> Task?
    {
        return realmService.realmConnection.object(ofType: Task.self, forPrimaryKey: id)
    }
    
    class func removeTask(WithID id : Int) {
        guard let task = realmService.realmConnection.object(ofType: Task.self, forPrimaryKey: id) else {
            return
        }
        
        realmService.realmConnection.delete(task)
    }
}

//MARK:- Hint
extension StorageManager
{
    class func getHint(WithID id : Int) -> Hint? {
        return realmService.realmConnection.object(ofType: Hint.self, forPrimaryKey: id)
    }
    
    class func removeHint(WithID id : Int) {
        guard let hint = realmService.realmConnection.object(ofType: Hint.self, forPrimaryKey: id) else {
            return
        }
        
        realmService.realmConnection.delete(hint)
    }
}

//MARK:- Answer
extension StorageManager {
    class func getAnswer(WithID id : Int) -> Answer? {
        return realmService.realmConnection.object(ofType: Answer.self, forPrimaryKey: id)
    }
    
    class func removeAnswer(WithID id : Int) {
        guard let answer = realmService.realmConnection.object(ofType: Answer.self, forPrimaryKey: id) else {
            return
        }
        
        realmService.realmConnection.delete(answer)
    }
}

//MARK:- Fact
extension StorageManager
{
    class func getFact(WithID id : Int) -> Fact? {
        return realmService.realmConnection.object(ofType: Fact.self, forPrimaryKey: id)
    }
    
    class func removeFact(WithID id : Int) {
        guard let fact = realmService.realmConnection.object(ofType: Fact.self, forPrimaryKey: id) else {
            return
        }
        
        realmService.realmConnection.delete(fact)
    }
}

//MARK:- Orders
extension StorageManager
{
    class func removeOrder(OrderID id : Int)
    {
        
    }
}
