//
//  ParseManager.swift
//  SmartQuest
//
//  Created by Pavel on 25/11/2017.
//  Copyright © 2017 Pavel Khanukov. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreLocation

class ParseManager
{
    static let realmService = RealmService.shared
}


//MARK:- City Parser
extension ParseManager
{
    final class func saveCity(FromJSON jsonData : JSON)
    {
        let cityID = jsonData[CityConstants.city_id].intValue
        let city = StorageManager.getCity(WithID: cityID) ?? City.create(WithID: cityID)
        
        realmService.writeBlock {
            city.name = jsonData[CityConstants.city_name].stringValue
            city.shortName = jsonData[CityConstants.city_short_name].stringValue
            city.imageURL = jsonData[CityConstants.city_image_url].stringValue
            saveCityNames(ForCity: city, jsonData: jsonData["city_names"])
            realmService.realmConnection.add(city, update: true)
        }
    }
    
    private class func saveCityNames(ForCity city : City, jsonData : JSON) {
        let keys = jsonData.dictionaryValue.keys
        city.listOfNames.removeAll()
        
        keys.forEach { (key) in
            let id = "\(city.id)" + key
            let value = jsonData[key].stringValue
            
            let cityName = StorageManager.getCityName(WithID: id) ?? CityName.create(WithID: id)
            
            cityName.langCode = key
            cityName.name = value
            city.listOfNames.append(cityName)
        }
    }
    
    
    fileprivate class func connectCityTo(Scenario scenario : Scenario, cityID : Int) {
        let city = StorageManager.getCity(WithID: cityID) ?? City.create(WithID: cityID)
        realmService.realmConnection.add(city, update: true)
        scenario.city = city
    }
    
    fileprivate class func connectCityTo(News news : News, cityID : Int)
    {
        let city = StorageManager.getCity(WithID: cityID) ?? City.create(WithID: cityID)
        realmService.realmConnection.add(city, update: true)
        news.city = city
    }
}

//MARK:- Orders Parser
extension ParseManager
{
    final class func saveOrders(Orders jsonData : [JSON]) {
        jsonData.forEach { (answerJSON) in
            saveOrder(Order: answerJSON)
        }
    }
    
    final class func saveOrder(Order jsonData : JSON) {
        let orderID = jsonData[OrderConstants.order_id].intValue
        
        let order = Order.createOrder(WithID: orderID)
        
        realmService.writeBlock {
            order.paymentID = jsonData[OrderConstants.payment_id].intValue
            order.status = jsonData[OrderConstants.status].intValue
            order.scenarioID = jsonData[OrderConstants.scenario_id].intValue
            realmService.realmConnection.add(order, update: true)
        }
    }
}

//MARK:- News Parser
extension ParseManager
{
    final class func saveNews(FromJSON jsonData : JSON)
    {
        let newsID = jsonData[NewsConstant.novelty_id].intValue
        let news = StorageManager.getNews(WithID: newsID) ?? News.create(WithID: newsID)
        
        realmService.writeBlock {
            news.imageURL = jsonData[NewsConstant.novelty_url].stringValue
            news.name = jsonData[NewsConstant.novelty_name].stringValue
            news.type = jsonData[NewsConstant.novelty_type].intValue
            
            let newsContent = jsonData[NewsConstant.novelty_content]
            news.text = newsContent[NewsConstant.text].stringValue
            
            if let scenarioID = newsContent[NewsConstant.scenarios].arrayValue.first?.intValue {
                connectScenarioTo(News: news, scenarioID: scenarioID)
            }
            
            if let cityID = jsonData[NewsConstant.novelty_city].int {
                connectCityTo(News: news, cityID: cityID)
            }
            
            if let langID = jsonData[NewsConstant.novelty_language_id].int {
                connectLanguageTo(News: news, langID: langID)
            }
            
            realmService.realmConnection.add(news, update: true)
        }
    }
}

//MARK:- Languages Parser
extension ParseManager
{
    final class func saveLanguage(FromJSON jsonData : JSON)
    {
        let langID = jsonData[LanguageConstants.language_id].intValue
        let language = StorageManager.getLanguage(WithID: langID) ?? Language.create(WithID: langID)
        realmService.writeBlock {
            language.code = jsonData[LanguageConstants.languageCode].stringValue
            language.name = language.code == "RU" ? "Русский" : jsonData[LanguageConstants.language_description].stringValue
            realmService.realmConnection.add(language, update: true)
        }
    }

    fileprivate class func connectLanguageTo(Scenario scenario : Scenario, langID : Int) {
        let language = StorageManager.getLanguage(WithID: langID) ?? Language.create(WithID: langID)
        realmService.realmConnection.add(language, update: true)
        scenario.lang = language
    }
    
    fileprivate class func connectLanguageTo(News news : News, langID : Int) {
        let language = StorageManager.getLanguage(WithID: langID) ?? Language.create(WithID: langID)
        realmService.realmConnection.add(language, update: true)
        news.language = language
    }
}

//MARK:- Scenario Parser
extension ParseManager
{
    //MARK:- Scenario
    
    fileprivate class func connectScenarioTo(News news : News, scenarioID : Int)
    {
        let scenario = StorageManager.getScenatio(WithID: scenarioID) ?? Scenario.create(WithID: scenarioID)
        realmService.realmConnection.add(scenario, update: true)
        news.scenario = scenario
    }
    
    final class func saveScenario(FromJSON jsonData : JSON) {
        let scenarioID = jsonData[ScenarioConstants.scenario_id].intValue
        let scenario = StorageManager.getScenatio(WithID: scenarioID) ?? Scenario.create(WithID: scenarioID)
        
        realmService.writeBlock {
            connectCityTo(Scenario: scenario, cityID: jsonData[ScenarioConstants.city_id].intValue)
            connectLanguageTo(Scenario: scenario, langID: jsonData[ScenarioConstants.scenario_lang].intValue)
            connectScenarioCategories(CategoryData: jsonData[ScenarioConstants.scenario_terms][ScenarioConstants.scenario_terms_tags].arrayValue, forScenario: scenario)
            scenario.fullDescription = jsonData[ScenarioConstants.scenario_description].stringValue
            scenario.shortDescription = jsonData[ScenarioConstants.scenario_short_description].stringValue
            scenario.name = jsonData[ScenarioConstants.scenario_name].stringValue
            scenario.imageUrl = jsonData[ScenarioConstants.scenario_image_url].stringValue
            scenario.difficult = jsonData[ScenarioConstants.scenario_difficult].intValue
            scenario.type = jsonData[ScenarioConstants.scenario_type].intValue
            scenario.price = jsonData[ScenarioConstants.scenario_price].doubleValue
            scenario.rate = jsonData[ScenarioConstants.scenario_rate].doubleValue
            scenario.votes = jsonData[ScenarioConstants.scenario_votes].intValue
            scenario.minimumPlayers = jsonData[ScenarioConstants.scenario_minimum_players].intValue
            scenario.distance = jsonData[ScenarioConstants.scenario_distance].intValue
            scenario.time = jsonData[ScenarioConstants.scenario_time].intValue
            
            scenario.coordinates.removeAll()
            scenario.coordinates.append(createLocation(FromJSON: jsonData))
            
            realmService.realmConnection.add(scenario, update: true)
        }
    }
    
    final class func saveUser(fromJSON jsonData: JSON)
    {
        let userId = jsonData[UserConstants.user_id].stringValue
        let user = StorageManager.getUser(WithID: userId) ?? User.create(WithID: userId)
        
        realmService.writeBlock {
            
            let cityID = jsonData[UserConstants.user_city_id].intValue
            let city = StorageManager.getCity(WithID: cityID) ?? City.create(WithID: cityID)
            
            user.city = city
            user.email = jsonData[UserConstants.user_email].stringValue
            
            let langID = jsonData[UserConstants.user_language_id].intValue
            let language = StorageManager.getLanguage(WithID: langID) ?? Language.create(WithID: langID)
            
            user.language = language
            user.phoneNumber = jsonData[UserConstants.user_phone].stringValue
            user.userName = jsonData[UserConstants.user_name].stringValue
            user.userType = jsonData[UserConstants.user_type].intValue
            
            realmService.realmConnection.add(user, update: true)
        }
    }
    
    private class func createLocation(FromJSON jsonData : JSON) -> Location
    {
        let ltt = jsonData[ScenarioConstants.scenario_start_point_latitude].doubleValue
        let lng = jsonData[ScenarioConstants.scenario_start_point_longitude].doubleValue
        
        let location = Location()
        location.latitude = ltt
        location.longitude = lng
        
        coordinates(forCoordinates: location.coordinate, forLocation: location)
        return location
    }
    
    
    class func coordinates(forCoordinates coordinates : CLLocationCoordinate2D, forLocation : Location) {
        
        let location = CLLocation.init(latitude: coordinates.latitude, longitude: coordinates.longitude)
        let geocoder = CLGeocoder()
        
        func parseStringFromPlacemark(_ placemark : CLPlacemark) {
            guard let adressDict = placemark.addressDictionary else { return }
            
            if adressDict.keys.contains("FormattedAddressLines") {
                guard let titles = adressDict["FormattedAddressLines"] as? [String] else { return }
                let title = titles.description.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "[", with: "").replacingOccurrences(of: "]", with: "")
                realmService.writeBlock {
                    forLocation.addressText = title
                }
            }
        }
        
        MapService.main.generateSnapshot(ForCoordinates: coordinates, distance: 1000, success: { (path) in
        })
    
        if #available(iOS 11.0, *) {
            geocoder.reverseGeocodeLocation(location, preferredLocale: Locale.current) { (placemarks, error) in
                guard let placemark = placemarks?.first else { return }
                parseStringFromPlacemark(placemark)
            }
        } else {
            geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
                guard let placemark = placemarks?.first else { return }
                parseStringFromPlacemark(placemark)
            }
        }
    }

    final class func updateExistScenraio(WithID scenarioID : Int, FromJSON jsonData : JSON)
    {
        let scenario = StorageManager.getScenatio(WithID: scenarioID) ?? Scenario.create(WithID: scenarioID)
    
        realmService.writeBlock {
            connectCityTo(Scenario: scenario, cityID: jsonData[ScenarioConstants.city_id].intValue)
            connectLanguageTo(Scenario: scenario, langID: jsonData[ScenarioConstants.scenario_lang].intValue)
            connectScenarioCategories(CategoryData: jsonData[ScenarioConstants.scenario_terms][ScenarioConstants.scenario_terms_tags].arrayValue, forScenario: scenario)
            scenario.name = jsonData[ScenarioConstants.scenario_name].stringValue
            scenario.fullDescription = jsonData[ScenarioConstants.scenario_description].stringValue
            scenario.type = jsonData[ScenarioConstants.scenario_type].intValue
            scenario.price = jsonData[ScenarioConstants.scenario_price].doubleValue
            scenario.difficult = jsonData[ScenarioConstants.scenario_difficult].intValue
            scenario.minimumPlayers = jsonData[ScenarioConstants.scenario_minimum_players].intValue
            scenario.imageUrl = jsonData[ScenarioConstants.scenario_image_url].stringValue
            
            let stages = jsonData[ScenarioConstants.stages].arrayValue
            saveStages(Stages: stages, forScenario: scenario)
            
            let facts = jsonData[ScenarioConstants.facts].arrayValue
            saveFacts(Facts: facts, forScenario: scenario)
            
            realmService.realmConnection.add(scenario, update: true)
        }
    }
    
    //MARK:- Scenario category
    final class func connectScenarioCategories(CategoryData jsonData : [JSON], forScenario scenario : Scenario) {
        debugPrint(jsonData)
        scenario.categories.removeAll()
        jsonData.forEach { (categoryID) in
            let category = saveScenarioCategory(jsonData: categoryID)
            scenario.categories.append(category)
        }
    }
    
    @discardableResult
    class func saveScenarioCategory(jsonData : JSON) -> ScenarioCategory {
        
        if jsonData.string != nil {
            let category = StorageManager.getScenarioCategory(WithID: jsonData.intValue) ?? ScenarioCategory.create(WithID: jsonData.intValue)
            return category
        } else if let categoryID = jsonData["scenario_type_id"].int {
            
            let category = StorageManager.getScenarioCategory(WithID: categoryID) ?? ScenarioCategory.create(WithID: categoryID)
            
            realmService.writeBlock {
                category.categoryDescription = jsonData["scenario_type_description"].stringValue
                saveCategoryNames(ForCategory: category, jsonData: jsonData["scenario_type_names"])
                realmService.realmConnection.add(category, update: true)
            }
            
            return category
        }
        
        return ScenarioCategory()
    }
    
    private class func saveCategoryNames(ForCategory category : ScenarioCategory, jsonData : JSON) {
        let keys = jsonData.dictionaryValue.keys
        category.listOfNames.removeAll()
        
        keys.forEach { (key) in
            let id = "\(category.id)" + key
            let value = jsonData[key].stringValue
            
            let catName = StorageManager.getScenarioCategoryName(WithID: id) ?? ScenarioCategoryNames.create(WithID: id)
            catName.langCode = key
            catName.name = value
            category.listOfNames.append(catName)
        }
    }
    
    //MARK:- Stage
    final class func saveStages(Stages jsonData : [JSON], forScenario scenario : Scenario)
    {
        jsonData.forEach { (stageJSON) in
            saveStage(Stage: stageJSON, forScenario: scenario)
        }
    }
    
    final class func saveEmptyStage(StageID stageID : Int, ForScenarioID scenarioID : Int) -> Stage
    {
        let stage = StorageManager.getStage(WithID: stageID) ?? Stage.create(WithID: stageID)
        let scenario = StorageManager.getScenatio(WithID: scenarioID) ?? Scenario.create(WithID: scenarioID)
        connectStageTo(Scenario: scenario, Stage: stage)
        
        realmService.realmConnection.add(stage, update: true)
        
        return stage
    }
    
    final class func saveStage(Stage jsonData : JSON, forScenario scenario : Scenario)
    {
        let stageID = jsonData[ScenarioConstants.stage_id].intValue
        let stage = StorageManager.getStage(WithID: stageID) ?? Stage.create(WithID: stageID)
        
        stage.name = jsonData[ScenarioConstants.stage_name].stringValue
        stage.fullDescription = jsonData[ScenarioConstants.stage_description].stringValue
        stage.type = jsonData[ScenarioConstants.stage_type].intValue
        stage.duration = jsonData[ScenarioConstants.stage_duration].stringValue
        stage.order = jsonData[ScenarioConstants.stage_order].intValue
        
        let tasks = jsonData[ScenarioConstants.tasks].arrayValue
        saveTasks(Tasks: tasks, forStage: stage)
        connectStageTo(Scenario: scenario, Stage: stage)
        
        realmService.realmConnection.add(stage, update: true)
    }
    
    private class func connectStageTo(Scenario scenario : Scenario, Stage stage : Stage)
    {
        let exist = scenario.stages.first(where: { (stageIn) -> Bool in
            return stageIn.id == stage.id
        })
        
        guard exist != nil else {
            scenario.stages.append(stage)
            return
        }
    }
    
    //MARK:- Task
    final class func saveTasks(Tasks jsonData : [JSON], forStage stage : Stage)
    {
        jsonData.forEach { (taskJSON) in
            saveTask(Task: taskJSON, forStage: stage)
        }
    }
    
    final class func saveTask(Task jsonData : JSON)
    {
        let stageID = jsonData[ScenarioConstants.stage_id].intValue
        let scenarioID = jsonData[ScenarioConstants.scenario_id].intValue
        
        realmService.writeBlock {
            let stage = StorageManager.getStage(WithID: stageID) ?? saveEmptyStage(StageID: stageID, ForScenarioID: scenarioID)
            saveTask(Task: jsonData, forStage: stage)
        }
    }
    
    final class func saveTask(Task jsonData : JSON, forStage stage : Stage)
    {
        let taskID = jsonData[ScenarioConstants.task_id].intValue
        let task = StorageManager.getTask(WithID: taskID) ?? Task.create(WithID: taskID)

        task.role_id = jsonData[ScenarioConstants.role_id].intValue
        task.duration = jsonData[ScenarioConstants.task_duration].stringValue
        task.order = jsonData[ScenarioConstants.task_order].intValue
        task.title = jsonData[ScenarioConstants.task_title].stringValue
        task.imageURL = jsonData[ScenarioConstants.task_image_url].stringValue
        task.type = jsonData[ScenarioConstants.task_type].intValue
        
        let wrongAnswers = jsonData[ScenarioConstants.task_answer_option_texts].arrayValue
        task.wrongAnswers.removeAll()
        wrongAnswers.forEach({ (wrongAnswer) in
            task.wrongAnswers.append(wrongAnswer.stringValue)
        })
        
        let sections = jsonData[ScenarioConstants.task_sections].arrayValue
    
        let hints = jsonData[ScenarioConstants.hints].arrayValue
        saveHits(Hints: hints, forTask: task)
        
        realmService.realmConnection.delete(task.sections)
        realmService.realmConnection.add(task, update: true)
        
        saveSections(Sections: sections, forTask: task)
        
        let answers = jsonData[ScenarioConstants.answer_options].arrayValue
        saveAnswers(Answers: answers, forTask: task)
        
        
        realmService.realmConnection.add(task, update: true)
        connectTaskTo(Stage: stage, Task: task)
    }
    
    private class func connectTaskTo(Stage stage : Stage, Task task : Task)
    {
        let exist = stage.tasks.first(where: { (taskIn) -> Bool in
            return taskIn.id == task.id
        })
        
        guard exist != nil else {
            stage.tasks.append(task)
            return
        }
    }
    
    //MARK:- Section
    final class func saveSections(Sections jsonData : [JSON], forTask task : Task)
    {
        jsonData.forEach { (sectionJSON) in
            let section = saveSection(Section: sectionJSON)
            task.sections.append(section)
        }
    }
    
    final class func saveSections(Sections jsonData : [JSON], forHint hint : Hint)
    {
        jsonData.forEach { (sectionJSON) in
            let section = saveSection(Section: sectionJSON)
            hint.sections.append(section)
        }
    }
    
    final class func saveSections(Sections jsonData : [JSON], forFact fact : Fact)
    {
        jsonData.forEach { (sectionJSON) in
            let section = saveSection(Section: sectionJSON)
            fact.sections.append(section)
        }
    }
    
    final class func saveSection(Section jsonData : JSON) -> Section
    {
        let section = Section()
        section.type = jsonData[ScenarioConstants.section_type].stringValue
        section.order = jsonData[ScenarioConstants.section_order].intValue
        
        let sectionData = jsonData[ScenarioConstants.section_data]
        saveLeftData(data: sectionData[ScenarioConstants.left], forSection: section)
        saveRightData(data: sectionData[ScenarioConstants.rigth], forSection: section)
        saveCenterData(data: sectionData[ScenarioConstants.center], forSection: section)
        
        return section
    }

    //MARK:- SectionData
    final class func saveLeftData(data : JSON, forSection : Section)
    {
        guard let section = SectionData.create(WithData: data) else { return }
        forSection.leftSide = section
    }
    
    final class func saveRightData(data : JSON, forSection : Section)
    {
        guard let section = SectionData.create(WithData: data) else { return }
        forSection.rightSide = section
    }
    
    final class func saveCenterData(data : JSON, forSection : Section)
    {
        guard let section = SectionData.create(WithData: data) else { return }
        forSection.centerSide = section
    }
    
    //MARK:- Fact
    final class func saveFacts(Facts jsonData : [JSON], forTask tast : Task? = nil, forScenario scenario : Scenario? = nil)
    {
        jsonData.forEach { (factJSON) in
            saveFact(Fact: factJSON, forTask: tast, forScenario: scenario)
        }
    }
    
    final class func saveFact(Fact jsonData : JSON, forTask task : Task?, forScenario scenario : Scenario?)
    {
        let factID = jsonData[ScenarioConstants.fact_id].intValue
        
        let fact = StorageManager.getFact(WithID: factID) ?? Fact.create(WithID: factID)
        
        fact.imageURL = jsonData[ScenarioConstants.fact_image_url].string ?? fact.imageURL
        fact.sections.removeAll()
        
        let sections = jsonData[ScenarioConstants.fact_sections].arrayValue
        saveSections(Sections: sections, forFact: fact)
        
        realmService.realmConnection.add(fact, update: true)
        
        guard let task = task else
        {
            let taskID = jsonData[ScenarioConstants.task_id].intValue
            StorageManager.getTask(WithID: taskID)?.fact = fact
            
            guard let scenario = scenario else { return }
            connectFact(Fact: fact, toScenario: scenario)
            
            return
        }
        
        task.fact = fact
    }
    
    private class func connectFact(Fact fact : Fact, toScenario scenario : Scenario)
    {
        guard scenario.facts.contains(fact) else
        {
            scenario.facts.append(fact)
            return
        }
    }
    
    //MARK:- Hits
    final class func saveHit(Hint jsonData : JSON) {
        let task = StorageManager.getTask(WithID: jsonData["task_id"].intValue) ?? Task.create(WithID: jsonData["task_id"].intValue)
        
        realmService.writeBlock {
            saveHit(Hint: jsonData, forTask: task)
            realmService.realmConnection.add(task, update: true)
        }
    }
    
    final class func saveHits(Hints jsonData : [JSON], forTask task : Task) {
        jsonData.forEach { (hintJson) in
            saveHit(Hint: hintJson, forTask: task)
        }
    }
    
    final class func saveHit(Hint jsonData : JSON, forTask task : Task) {
        let hintID = jsonData[ScenarioConstants.hint_id].intValue
        let hint = StorageManager.getHint(WithID: hintID) ?? Hint.create(WithID: hintID)
        
        hint.type = jsonData[ScenarioConstants.hint_type].intValue
        hint.order = jsonData[ScenarioConstants.hint_order].intValue
        hint.imageURL = jsonData[ScenarioConstants.hint_image_url].string
        
        hint.sections.removeAll()
        let sections = jsonData[ScenarioConstants.hint_sections].arrayValue
        saveSections(Sections: sections, forHint: hint)
        
        guard task.hints.contains(hint) else {
            task.hints.append(hint); return
        }
    }
    
    //MARK:- Answer
    
    final class func saveAnswer(Answers jsonData : JSON) {
        let task = StorageManager.getTask(WithID: jsonData["task_id"].intValue) ?? Task.create(WithID: jsonData["task_id"].intValue)
        
        realmService.writeBlock {
            saveAnswer(Answer: jsonData, task: task)
            realmService.realmConnection.add(task, update: true)
        }
    }
    
    final class func saveAnswers(Answers jsonData : [JSON], forTask : Task) {
        jsonData.forEach { (answerJSON) in
            saveAnswer(Answer: answerJSON, task: forTask)
        }
    }
    
    final class func saveAnswer(Answer jsonData : JSON, task : Task) {
        let answerID = jsonData[ScenarioConstants.answer_option_id].intValue
        let answer = task.answers.first(where: { (answerIn) -> Bool in
            return answerIn.id == answerID
        }) ?? StorageManager.getAnswer(WithID: answerID) ?? Answer.create(WithID: answerID)
    
        answer.text = jsonData[ScenarioConstants.answer_option_text].string
        answer.specialText = jsonData[ScenarioConstants.answer_option_special_text].string
        answer.type = jsonData[ScenarioConstants.answer_option_type].intValue
        answer.isRight = jsonData[ScenarioConstants.answer_option_is_right].boolValue
        
        guard task.answers.contains(answer) else {
            task.answers.append(answer); return
        }
    }
}



