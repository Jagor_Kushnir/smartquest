//
//  UpdateManager.swift
//  SmartQuest
//
//  Created by Pavel on 21/01/2018.
//  Copyright © 2018 Pavel Khanukov. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

private enum UpdateEntity : String {
    case news         = "news"
    case languages    = "languages"
    case scenarios    = "scenarios"
    case cities       = "cities"
    case orders       = "orders"
    case tasks        = "tasks"
    case answer       = "answer_options"
    case hints        = "hints"
    case scenarioCategories = "scenario_types"
    case unknown      = "unknown"
}

private enum UpdateType : Int
{
    case unknown = -1
    case delete  = 0
    case insert  = 1
    case update  = 2
}

private let update_id        = "update_id"
private let update_type      = "update_type"
private let update_entity    = "update_entity"
private let update_entity_id = "update_entity_id"
private let update_data      = "update_data"
private let created_at       = "created_at"
private let updated_at       = "updated_at"
private let next_step        = "next_step"

class UpdateManager
{
    private static var loadMore : Bool = false
    private static var offset = 0
    static var countPerUpdate = 100
    
    class func checkForUpdates(successBlock : @escaping SuccessBlock)
    {
        APIManager.getUpdates(ItemsCount: countPerUpdate, withOffset: offset) { (result, error) in
            guard let resultJSON = result else {
                loadMore = false
                offset = 0
                successBlock(false, error)
                return
            }
            
            parse(Update: resultJSON)
            
            guard loadMore else { successBlock(true, nil); return }
            self.checkForUpdates(successBlock: successBlock)
        }
    }
    
    class func saveUser(successBlock: @escaping SuccessBlock)
    {
        APIManager.getUser(withId: LoginManager.getUserId()) { (result, error) in
            guard let resultJSON = result else {
                successBlock(false, error)
                return
            }
            
            ParseManager.saveUser(fromJSON: resultJSON)
            successBlock(true, nil)
        }
    }
    
    static var count : Int = 0
    private class func parse(Update updateJSON : JSON) {
        guard let data = updateJSON[APIManager.kData].array else {
            loadMore = false
            offset = 0
            debugPrint("Nothing to update", updateJSON); return
            debugPrint(count)
        }
        
        
        UpdateManager.count += data.count
        if updateJSON[next_step].intValue == 1 {
            loadMore = true
            offset += countPerUpdate
        } else {
            loadMore = false
            offset = 0
            debugPrint("Last updates", updateJSON);
            debugPrint(count)
        }

        
        data.forEach { (updateJSON) in
            
            debugPrint("[CORE]","[UPDATE_TYPE]",updateJSON[update_entity].stringValue)
            let updateType = detectUpdateType(type: updateJSON[update_type].intValue)
            
            switch updateType
            {
            case .delete: prepareToDelete(data: updateJSON)
            case .insert: prepareToInsert(data: updateJSON)
            case .update: prepareToUpdate(data: updateJSON)
            case .unknown: debugPrint(data)
            }
        }
    }
    
    private class func prepareToDelete(data : JSON)
    {
        let entityType = detectUpdateEntity(entityName: data[update_entity].stringValue)
        let entityID : Int = data[update_entity_id].intValue
        switch entityType
        {
        case .scenarios: StorageManager.removeScenario(entityID)
        case .cities: StorageManager.removeCity(entityID)
        case .languages: StorageManager.removeLanguage(entityID)
        case .news: StorageManager.removeNews(entityID)
        case .tasks: StorageManager.removeTask(WithID: entityID)
        case .orders: StorageManager.removeOrder(OrderID: entityID)
        case .unknown: break
        case .answer: StorageManager.removeAnswer(WithID: entityID)
        case .hints: StorageManager.removeHint(WithID: entityID)
        case .scenarioCategories: StorageManager.removeScenarioCategory(entityID)
        }
    }
    
    private class func prepareToInsert(data : JSON)
    {
        let entityType = detectUpdateEntity(entityName: data[update_entity].stringValue)
        
        switch entityType
        {
        case .scenarios: ParseManager.saveScenario(FromJSON: data[update_data])
        case .cities: ParseManager.saveCity(FromJSON: data[update_data])
        case .languages: ParseManager.saveLanguage(FromJSON: data[update_data])
        case .news: ParseManager.saveNews(FromJSON: data[update_data])
        case .tasks: ParseManager.saveTask(Task: data[update_data])
        case .orders: ParseManager.saveOrder(Order: data[update_data])
        case .answer: ParseManager.saveAnswer(Answers: data[update_data])
        case .hints: ParseManager.saveHit(Hint: data[update_data])
        case .scenarioCategories: ParseManager.saveScenarioCategory(jsonData: data[update_data])
        case .unknown: break
        }
    }
    
    private class func prepareToUpdate(data : JSON)
    {
        let entityType = detectUpdateEntity(entityName: data[update_entity].stringValue)
        
        switch entityType
        {
        case .scenarios: ParseManager.saveScenario(FromJSON: data[update_data])
        case .cities: ParseManager.saveCity(FromJSON: data[update_data])
        case .languages: ParseManager.saveLanguage(FromJSON: data[update_data])
        case .news: ParseManager.saveNews(FromJSON: data[update_data])
        case .tasks: ParseManager.saveTask(Task: data[update_data])
        case .orders: ParseManager.saveOrder(Order: data[update_data])
        case .unknown: break
        case .answer: ParseManager.saveAnswer(Answers: data[update_data])
        case .hints: ParseManager.saveHit(Hint: data[update_data])
        case .scenarioCategories: ParseManager.saveScenarioCategory(jsonData: data[update_data])
        }
    }
    
    private class func detectUpdateType(type : Int) -> UpdateType {
       return UpdateType.init(rawValue: type) ?? .unknown
    }
    
    private class func detectUpdateEntity(entityName : String) -> UpdateEntity
    {
        return UpdateEntity.init(rawValue: entityName) ?? .unknown
    }
}

//MARK:- Update API
extension APIManager
{
    fileprivate class func getUpdates(ItemsCount count : Int, withOffset offset : Int, success : @escaping ResultBlock)
    {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let url = updateURL(ForItems: count, withOffset: offset)
        Alamofire.request(url, method: .get, headers: headers())
            .validate(contentType: ["application/json"])
            .responseJSON { (response) in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                switch response.result
                {
                case .success(let result):
                    let resultJSON = JSON(result)
                    success(resultJSON, nil)
                case.failure(let error):
                    success(nil, error)
                    debugPrint(error.localizedDescription)
                }
        }
    }
}
