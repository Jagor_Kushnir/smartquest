//
//  QuestConstants.swift
//  SmartQuest
//
//  Created by Pavel on 05/01/2018.
//  Copyright © 2018 Pavel Khanukov. All rights reserved.
//

import Foundation

enum ScenarioType : Int
{
    case standart = 4
    case new = 1
    case promo = 2
}

enum ScenarioTypeName : String {
    case standart = "Стандартный"
    case new = "Новый"
    case promo = "Промо"
}

class NewsConstant
{
    static let novelty_start_date = "novelty_start_date"
    static let novelty_end_date = "novelty_end_date"
    static let novelty_url = "novelty_url"
    static let novelty_city = "novelty_city"
    static let novelty_content = "novelty_content"
    static let text = "text"
    static let scenarios = "scenarios"
    static let novelty_type = "novelty_type"
    static let novelty_id = "novelty_id"
    static let novelty_language_id = "novelty_language_id"
    static let novelty_name = "novelty_name"
}

class LanguageConstants
{
    static let languageCode = "language"
    static let language_id = "language_id"
    static let language_description = "language_description"
}

class CityConstants
{
    static let city_id = "city_id"
    static let city_short_name = "city_short_name"
    static let city_name = "city_name"
    static let city_image_url = "city_image_url"
}

//MARK: - Order
class OrderConstants
{
    static let payment_id = "payment_id"
    static let status = "status"
    static let order_id = "order_id"
    static let updated_at = "updated_at"
    static let created_at = "created_at"
    static let user_id = "user_id"
    static let scenario_id = "scenario_id"
    static let order_day = "order_day"
}

//MARK: - USER
class UserConstants
{
    static let user_id = "user_id"
    static let user_name = "user_name"
    static let user_email = "user_email"
    static let user_city_id = "user_city_id"
    static let user_language_id = "user_language_id"
    static let user_phone = "user_phone"
    static let user_type = "user_type"
}

//MARK:- Keys
class ScenarioConstants
{
    static let scenario_name = "scenario_name"
    static let scenario_description = "scenario_description"
    static let scenario_short_description = "scenario_short_description"
    static let scenario_type = "scenario_type"
    static let scenario_price = "scenario_price"
    static let scenario_terms = "scenario_terms"
    static let scenario_difficult = "scenario_difficult"
    static let scenario_start_date = "scenario_start_date"
    static let scenario_end_date = "scenario_end_date"
    static let scenario_minimum_players = "scenario_minimum_players"
    static let scenario_start_point_latitude = "scenario_start_point_latitude"
    static let scenario_start_point_longitude = "scenario_start_point_longitude"
    static let scenario_image_url = "scenario_image_url"
    static let scenario_lang = "scenario_lang"
    static let scenario_rate = "scenario_rate"
    static let scenario_votes = "scenario_votes"
    static let scenario_distance = "scenario_distance"
    static let scenario_time = "scenario_time"
    static let city_id = "city_id"
    static let stages = "stages"
    static let facts = "facts"
    
    static let stage_id = "stage_id"
    static let stage_name = "stage_name"
    static let stage_description = "stage_description"
    static let stage_type = "stage_type"
    static let stage_duration = "stage_duration"
    static let stage_order = "stage_order"
    static let tasks = "tasks"
    
    static let task_id = "task_id"
    static let task_type = "task_type"
    static let task_sections = "task_sections"
    static let task_duration = "task_duration"
    static let task_order = "task_order"
    static let role_id = "role_id"
    static let task_title = "task_title"
    static let scenario_id = "scenario_id"
    static let scenario_terms_tags = "tags"
    
    static let task_image_url = "task_image_url"
    static let task_answer_option_texts = "task_answer_option_texts"
    static let task_coords = "task_coords"
    static let hints = "hints"
    static let answer_options = "answer_options"
    static let fact = "fact"
    
    static let section_data  = "section_data"
    static let section_type  = "section_type"
    static let section_order = "section_order"
    
    static let left = "left"
    static let rigth = "right"
    static let center = "center"
    
    static let text = "text"
    
    static let content_url = "content_url"
    static let content_note = "content_note"
    static let content_type = "content_type"
    
    static let hint_id = "hint_id"
    static let hint_sections = "hint_sections"
    static let hint_type = "hint_type"
    static let hint_order = "hint_order"
    static let hint_image_url = "hint_image_url"
    
    static let answer_option_id = "answer_option_id"
    static let answer_option_text = "answer_option_text"
    static let answer_option_type = "answer_option_type"
    static let answer_option_is_right = "answer_option_is_right"
    static let answer_option_special_text = "answer_option_special_text"
    
    static let fact_id = "fact_id"
    static let fact_sections = "fact_sections"
    static let fact_image_url = "fact_image_url"
}
